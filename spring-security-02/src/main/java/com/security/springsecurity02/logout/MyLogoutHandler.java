package com.security.springsecurity02.logout;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class MyLogoutHandler implements LogoutHandler, InitializingBean {
    private String msg = "";
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        System.out.println("=============================================");
        System.out.println(msg);
        System.out.println("=============================================");

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        msg = "我是登出的一个处理器";
    }
}
