package com.oauth2.oauth2demo01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2Demo01Application {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2Demo01Application.class, args);
    }

}
