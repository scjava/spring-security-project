package com.oauth2.oauth2demo01.handler;

import org.springframework.security.oauth2.provider.endpoint.DefaultRedirectResolver;
import org.springframework.stereotype.Component;

@Component
public class CustomizationRedirectResolver extends DefaultRedirectResolver {

    @Override
    protected boolean redirectMatches(String requestedRedirect, String redirectUri) {
        boolean matches = super.redirectMatches(requestedRedirect, redirectUri);
        return matches ? true : verification2verification(requestedRedirect, redirectUri);
    }

    private boolean verification2verification(String requestedRedirect, String redirectUri) {
        return  false;
    }

}
