package com.oauth2.oauth2demo01.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.plugin.liveconnect.SecurityContextHelper;

@RestController
@RequestMapping("/index")
public class IndexController {

    @RequestMapping("/test")
    public String test(){
        return "hello spring secruity oauth2s";
    }


    @RequestMapping("/getAuth")
    public Object getAuth(Authentication authentication){
       return authentication.getPrincipal();
    }
}