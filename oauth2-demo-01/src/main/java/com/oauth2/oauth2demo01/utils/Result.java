package com.oauth2.oauth2demo01.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private T data;

    private int code = HttpStatus.OK
            .value();

    private String msg;

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result() {
    }

    public Result(T data) {
        this.data = data;
    }

    public Result(T data, String msg) {
        this.data = data;
        this.msg = msg;
    }

    public Result(T data, int code, String msg) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public static <T> Result<T> ofSuccess() {
        return new Result<>(null, 200, "");
    }

    public static <T> Result<T> ofSuccess(T data) {
        return new Result<>(data, 200, "");
    }
    public String buildResultJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", Integer.valueOf(this.code));
        jsonObject.put("msg", this.msg);
        jsonObject.put("data", this.data);
        return JSON.toJSONString(jsonObject, new SerializerFeature[] { SerializerFeature.DisableCircularReferenceDetect });
    }
    public static <T> Result<T> ofSuccess(String message, T data) {
        return new Result<>(data, 200, message);
    }

    public static <T> Result<T> ofFail(int code, String msg) {
        return new Result<>(null, code, msg);
    }
    public static <T> Result<T> ofFail(int code, T data) {
        return new Result<>(data, code, "");
    }


    public static <T> Result<T> ofFail(int code, String msg, T data) {
        return new Result<>(data, code, msg);
    }



    @Override
    public String toString() {
        return "Result{, data=" + this.data + ", code='" + this.code + '\'' + ", msg='" + this.msg + '\'' + '}';
    }
}

