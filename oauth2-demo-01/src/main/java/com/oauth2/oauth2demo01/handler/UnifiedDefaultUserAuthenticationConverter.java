package com.oauth2.oauth2demo01.handler;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UnifiedDefaultUserAuthenticationConverter extends DefaultUserAuthenticationConverter {
    @Override
    public Map<String, Object> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> resultMap = (Map<String, Object>) super.convertUserAuthentication(authentication);
        resultMap.put("code", Integer.valueOf(HttpStatus.OK.value()));
        resultMap.put("msg", HttpStatus.OK.getReasonPhrase());
        return resultMap;
    }
}
