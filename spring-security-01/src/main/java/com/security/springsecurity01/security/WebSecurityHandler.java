package com.security.springsecurity01.security;

import com.security.springsecurity01.service.MySessionInformationExpiredStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

//@Configuration
public class WebSecurityHandler extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService myUserDetailsService;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//      auth.inMemoryAuthentication().withUser("admin")
//              .password(passwordEncoder.encode("123456")).roles().authorities("admin1")
//              .and()
//              .withUser("bml").passwouser/loginrd(passwordEncoder.encode("111111")).authorities("admin2");
        auth.userDetailsService(myUserDetailsService);
    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.formLogin().loginPage("/login.html")
//                .loginProcessingUrl("/user/login")
//                .defaultSuccessUrl("/main.html")
//                .and().authorizeRequests()
//                .antMatchers("/login.html","/error.html","/user/login").permitAll()
//                .anyRequest().authenticated();
//        http.csrf().disable();
//    }


//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.formLogin()
//                .loginPage("/login.html")
//                .loginProcessingUrl("/user/login")
//                .successForwardUrl("/user/toSuc")
//                .failureForwardUrl("/user/toError")
//                .and().authorizeRequests()
//                .antMatchers("/login.html","/user/login","/user/toError","/error.html").permitAll()
//                .anyRequest().authenticated();
//        http.csrf().disable();
//    }


//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//
//        http.formLogin()
//                .loginPage("/login.html")
//                .loginProcessingUrl("/user/login")
//                .successHandler(new MySuccHandler("/main.html"))
//                .failureHandler(new MyFailHandler("/error.html"))
//                .and().authorizeRequests()
//                .antMatchers("/login.html","/user/login","/error.html").permitAll()
//                .anyRequest().authenticated()
//                .and().csrf().disable();
//    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/user/showLogin")
                .loginProcessingUrl("/login")
                .successHandler(new MySuccHandler("/main.html"))
               .failureHandler(new MyFailHandler("/error.html"))
                .and().authorizeRequests()
                .antMatchers("/user/showLogin","/login","/error.html","/user/invdate").permitAll()
                .anyRequest().authenticated();
        //session的策略
        /*always :如果session不存在总是需要创建;
        ifRequired:如果需要就创建一个session（默认）登录时;
        never:Spring Security 将不会创建session，但是如果应用中其他地方创建了session，那
        么Spring Security将会使用它;
        stateless:Spring Security将绝对不会创建session，也不使用session。并且它会暗示不使用
        cookie，所以每个请求都需要重新进行身份验证。这种无状态架构适用于REST API
        及其无状态认证机制。*/
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
        //session超时
        http.sessionManagement().invalidSessionUrl("/user/invdate");
        //设置只能由一个用户可以进行登录，后面的用户登录会挤兑下线
        http.sessionManagement().maximumSessions(1)
                .expiredSessionStrategy(new MySessionInformationExpiredStrategy())
        //配置不允许挤兑下线
        .maxSessionsPreventsLogin(true);
    }
}
