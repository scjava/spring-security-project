package com.security.springsecurity02.config;

import com.security.springsecurity02.handler.MyAccessDeniedHandler;
import com.security.springsecurity02.logout.MyLogoutHandler;
import com.security.springsecurity02.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.vote.ConsensusBased;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableGlobalMethodSecurity(prePostEnabled = true,jsr250Enabled = true,securedEnabled = true)
public class AnnotationConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Autowired
    private MyLogoutHandler myLogoutHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin();
        http.authorizeRequests()
                .antMatchers("/psot.html").permitAll()
                .anyRequest().authenticated();


        http.logout().addLogoutHandler(myLogoutHandler);

        //http.csrf().disable();
        http.exceptionHandling().accessDeniedHandler(new MyAccessDeniedHandler());
        http.userDetailsService(userService);
    }
}
