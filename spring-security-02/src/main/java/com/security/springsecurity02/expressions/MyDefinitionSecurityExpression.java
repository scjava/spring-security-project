package com.security.springsecurity02.expressions;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Component
public class MyDefinitionSecurityExpression implements  DefinitionSecurityExpression {
    @Override
    public boolean hashPermission(HttpServletRequest request, Authentication authentication) {
        Object obj = authentication.getPrincipal();
        if(obj instanceof UserDetails){
            UserDetails details = (UserDetails) obj;
            Collection<? extends GrantedAuthority> authorities = details.getAuthorities();
            String name = request.getParameter("name");
            //判断用户得到的用户是否在权限内
            return authorities.contains(new SimpleGrantedAuthority(name));

        }
 
        return false;
    }
}
