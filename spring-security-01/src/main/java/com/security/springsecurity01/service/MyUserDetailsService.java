package com.security.springsecurity01.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        //第一种方式
//
//        String pwd = BCrypt.hashpw("123456",BCrypt.gensalt());
//
//        UserDetails userDetails = new User("bml","{bcrypt}"+pwd, AuthorityUtils.commaSeparatedStringToAuthorityList("bml,admin"));


        System.out.println("username="+s);
        //第二种方式
         String pwd = passwordEncoder.encode("111111");
         UserDetails userDetails = new User("bml",pwd, AuthorityUtils.commaSeparatedStringToAuthorityList("bml,admin"));
        return userDetails;
    }


}
