package com.security.springsecurity02.expressions;


import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface DefinitionSecurityExpression {

    boolean hashPermission(HttpServletRequest request, Authentication authentication);
}
