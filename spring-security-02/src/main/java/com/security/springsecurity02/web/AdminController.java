package com.security.springsecurity02.web;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RequestMapping("/admin")
@RestController
public class AdminController {

    @RequestMapping("/demo")
    @RolesAllowed({"r11"})//demo这个方法只允许就有r1角色的用户访问,可以配置多个
    public String demo(){
        return "hello spring security";
    }

    //@Secured需要完整的 角色名称
    @Secured({"ROLE_r1","ROLE_r2"})
        @RequestMapping("/main")
    public String main(){



        return "enter main....";
    }

    @RequestMapping("/query")
    @PreAuthorize("hasAnyRole('r1')")
    public String query(){

        return "enter query....";
    }
    @PreAuthorize("hasAnyAuthority('a12')")
    @RequestMapping("/test")
    public String test(){

        return "enter test....";
    }

    //限制只能查询自己的信息
    @PreAuthorize("principal.username.equals(#name)")
    @RequestMapping("/tone")
    public String tone(@RequestParam(name = "name") String name){
        return name;
    }



}
