package com.security.springsecurity01.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class LoginSucceLinstener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {
    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent interactiveAuthenticationSuccessEvent) {
        //登录成功过后，框架底层就将认证信息作为事件源，所以getsource可以得到这个事件源
        Authentication source = (Authentication) interactiveAuthenticationSuccessEvent.getSource();
        System.out.println("监听用户认证成功的事件，登录用户="+source.getName());
    }
}
