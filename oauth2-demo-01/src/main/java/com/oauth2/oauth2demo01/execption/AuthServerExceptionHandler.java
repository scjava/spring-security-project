package com.oauth2.oauth2demo01.execption;

import com.oauth2.oauth2demo01.utils.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;

public class AuthServerExceptionHandler extends DefaultWebResponseExceptionTranslator {
    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
        OAuth2Exception body = (OAuth2Exception)responseEntity.getBody();
        String oAuth2ErrorCode = body.getOAuth2ErrorCode();
        String errorMsg = body.getMessage();
        Result<ResponseErrorMsg> newBody = Result.ofFail(9999, new ResponseErrorMsg(oAuth2ErrorCode, errorMsg, Integer.valueOf(body.getHttpErrorCode())));
        return ResponseEntity.ok(newBody);
    }

    static class ResponseErrorMsg{

        public ResponseErrorMsg(String oAuth2ErrorCode, String errorMsg, Integer valueOf) {
        }
    }

}