package com.security.springsecurity01.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MySuccHandler implements AuthenticationSuccessHandler {

    private String page;

    public MySuccHandler(String page){
        this.page = page;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        System.out.println("登录成功，自定义逻辑，然后转发成功页面");
        response.sendRedirect(page);
    }
}
